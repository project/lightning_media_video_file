lightning_media_video_file
==========================

Installation
------------

Install this odule on top of a Drupal Lightning-based site (https://www.drupal.org/project/lightning) 
to enable locally-hosted video files in the Lightning Media system.

Current Maintainers
------------------

Alexander O'Neill (https://drupal.org/u/alxp)

